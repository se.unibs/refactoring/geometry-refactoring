package it.unibs.ing.se.refactoring.geometry;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.util.Scanner;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;

public class GeometryAppTest extends EndToEndTest {
	@Test
	public void shouldExit() throws Exception {
		Scanner input = buildInput("0");
		GeometryApp app = new GeometryApp(input, new PrintWriter(new ByteArrayOutputStream()));
		app.run();
	}

	@Test
	public void shouldPrintPolygonListAndExit() throws Exception {
		Scanner input = buildInput("1", "0");
		GeometryApp app = new GeometryApp(input, writer);
		app.run();
		assertThat(output(), containsString("Polygon list:"));
	}

	@Test
	public void shouldPrintNewPolygonMenuAndExit() throws Exception {
		Scanner input = buildInput("2", "0", "0");
		GeometryApp app = new GeometryApp(input, writer);
		app.run();
		assertThat(output(), containsString("Add new Polygon:"));
	}
}
